from django.urls import path
from .views import *
from MainApp import views

urlpatterns = [
   
    path('signup',views.signup,name='signup'),
    path('',views.home,name="home"),
    path('log',views.login_user,name="login"),
    path('uout',views.logout_user,name="u"),
    path('dash1',views.dashboard1,name="val"),
    path('dash2',views.dashboard2,name="val2"),
    path('teach',views.addteacher,name='add'),
    path('getall',views.getuser,name="all"),
    path('del<int:userid>',views.delteacher,name="dl"),
    path('edit<int:userid>',views.update_teacher,name="ed"),
    path('tlog',views.login_teacher,name="t"),
    path('tout',views.logout_teacher,name="l"),
    path('getview<str:userid>',views.getview,name="v"),
    path('dem',views.demo,name="d"),
    path('sear<str:userid>',views.searchstud,name="s"),
    path('vide<str:userid>',views.videoadd,name="a"),
    path('us',views.userhome,name="user"),
    path('slog',views.login_student,name="slogin"),
    path('tea',views.teacherslist,name="t"),
    path('course',views.courselist,name="c"),
   

]
