from django.db import models
from django.contrib.auth.models import AbstractUser,BaseUserManager
from django.contrib.auth.hashers import make_password

class UserManager(BaseUserManager):
    def _create_user(self, email, password, **other_fields):
        """
        Create and save a user with the given email and password. And any other fields, if specified.
        """
        if not email:
            raise ValueError('An Email address must be set')
        email = self.normalize_email(email)
        
        user = self.model(email=email, **other_fields)
        user.password = make_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **other_fields):
        other_fields.setdefault('is_staff', False)
        other_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **other_fields)

    def create_superuser(self, email, password=None, **other_fields):
        other_fields.setdefault('is_staff', True)
        other_fields.setdefault('is_superuser', True)

        if other_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if other_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **other_fields)
    

class User(AbstractUser):
    username=models.CharField(max_length=100,null=True,blank=True)
    first_name=models.CharField(max_length=100,null=True,blank=True)
    last_name=models.CharField(max_length=100,null=True,blank=True)
    email = models.EmailField(max_length=255, unique=True)
    phone = models.IntegerField(null=True)


    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['phone']
    objects=UserManager()

    def get_username(self):
        return self.email 

# Create your models here.

class department(models.Model):
    ch=(("COMMERSE & MANAGEMENT","COMMERSE & MANAGEMENT"),("COMPUTER APPLICATION","COMPUTER APPLICATION"),("ENGLISH LITERATURE","ENGLISH LITERATURE"))
    depart=models.CharField(max_length=40,null=True,choices=ch)
    def __str__(self):
        return self.depart


class teachers(models.Model):
    fname=models.CharField(max_length=40,null=True)
    lname=models.CharField(max_length=40,null=True)
    email=models.EmailField(max_length=30,null=True)
    profile_pic=models.ImageField(upload_to='images/',null=True)
    mob=models.IntegerField(null=True)
    pas=models.CharField(max_length=12,null=True)
    dname=models.ForeignKey('department',on_delete=models.CASCADE,null=True)
    def __str__(self):
        return self.fname


class videos(models.Model):
    depname=models.ForeignKey('department',on_delete=models.CASCADE,null=True)
    ch=(("FIRST SEMESTER","FIRST SEMESTER"),("SECOND SEMESTER","SECOND SEMESTER"),("THIRD SEMESTER","THIRD SEMESTER"),("FOURTH SEMESTER","FOURTH SEMESTER"),("FIFTH SEMESTER","FIFTH SEMESTER"),("SIXTH SEMESTER","SIXTH SEMESTER"))
    sem=models.CharField(max_length=40,null=True,choices=ch)
    vi=models.FileField(upload_to='videos/',null=True)
    def __str__(self):
        return self.sem



    

