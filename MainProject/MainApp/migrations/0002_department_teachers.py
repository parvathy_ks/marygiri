# Generated by Django 3.2.9 on 2022-02-03 05:12

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('MainApp', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='department',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('depart', models.CharField(choices=[('COMMERSE & MANAGEMENT', 'COMMERSE & MANAGEMENT'), ('COMPUTER APPLICATION', 'COMPUTER APPLICATION'), ('ENGLISH LITERATURE', 'ENGLISH LITERATURE')], max_length=25, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='teachers',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fname', models.CharField(max_length=40, null=True)),
                ('lname', models.CharField(max_length=40, null=True)),
                ('email', models.EmailField(max_length=30, null=True)),
                ('profile_pic', models.ImageField(null=True, upload_to='images/')),
                ('mob', models.IntegerField(null=True)),
                ('pas', models.CharField(max_length=12, null=True)),
                ('dname', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='MainApp.department')),
            ],
        ),
    ]
