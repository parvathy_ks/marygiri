# Generated by Django 4.0 on 2022-02-17 06:10

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('MainApp', '0012_delete_student'),
    ]

    operations = [
        migrations.CreateModel(
            name='videos',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sem', models.CharField(choices=[('FIRST SEMESTER', 'FIRST SEMESTER'), ('SECOND SEMESTER', 'SECOND SEMESTER'), ('THIRD SEMESTER', 'THIRD SEMESTER'), ('FOURTH SEMESTER', 'FOURTH SEMESTER'), ('FIFTH SEMESTER', 'FIFTH SEMESTER')], max_length=40, null=True)),
                ('video', models.CharField(max_length=60, null=True)),
                ('depname', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='MainApp.department')),
                ('teach', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='MainApp.teachers')),
            ],
        ),
    ]
