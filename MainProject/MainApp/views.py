from django.shortcuts import render,redirect
from django.http.response import HttpResponse
from.models import *
from django.core.files.storage import FileSystemStorage
from django.contrib.auth import login,logout,authenticate
from student_app.models import *


def signup(request):
    if request.method=="POST":
        fn=request.POST.get('fname')
        ln=request.POST.get('lname')
        em=request.POST.get('email')
        us=request.POST.get('username')
        pw=request.POST.get('password')
        User.objects.create_user(first_name=fn,last_name=ln,username=us,email=em,password=pw)
        return render(request,"index.html")
    return render(request,"signup.html")


def login_user(request):
    if request.method=="POST":
        em=request.POST.get('email')
        pw=request.POST.get('password')
        user=authenticate(request,email=em,password=pw)
        if user:
            login(request,user)
            return render(request,"index1.html")
        else:
           return HttpResponse("Invalid User!!")
    return render(request,"login.html")
def logout_user(request):
    return redirect('home')


def home(request):
    return render(request,"index.html")

def dashboard1(request):
    return render(request,"index1.html")
def dashboard2(request):
    return render(request,"index2.html")
def demo(request):
    return render(request,"dash.html")


def addteacher(request):
    a=department.objects.all()
    if request.method=='POST':
        f=request.POST.get('fname')
        l=request.POST.get('lname')
        d=request.POST.get('department')
        e=request.POST.get('email')
        ph=request.POST.get('mobile')
        im=request.FILES['img']
        fo=FileSystemStorage()
        im2=fo.save(im.name,im)
        p=request.POST.get('password')
        de=department.objects.get(id=d)
        teachers.objects.create(fname=f,lname=l,email=e,mob=ph,dname=de,pas=p,profile_pic=im2)
        return redirect("all")
    return render(request,"addteacher.html",{"data":a})
def getuser(request):
    k=teachers.objects.all()
    if request.method=="POST":
        se=request.POST.get('name')
        res=teachers.objects.filter(name=se)
        return render(request,"list.html",{"data":res})
    return render(request,"list.html",{"data":k})

def delteacher(request,userid):
    o=teachers.objects.get(id=userid)
    o.delete()
    return redirect("all")

def update_teacher(request,userid):
    x=teachers.objects.filter(id=userid).values()
    k=department.objects.all()
    if request.method=="POST":
        F=request.POST.get('f_name')
        L=request.POST.get('l_name')
        M=request.POST.get('mobile')
        E=request.POST.get('email') 
        D=request.POST.get('department')
        P=request.POST.get('password')  
        x.update(fname=F,lname=L,mob=M,email=E,dname=D,pas=P)
        return redirect("all")
    return render(request,"edit.html",{"userdata":x[0],"id":userid,"data":k})

def login_teacher(request):
    if request.method=="POST":
        em=request.POST.get('email')
        p=request.POST.get('password')
        if teachers.objects.filter(email=em,pas=p).exists():
            k=teachers.objects.filter(email=em,pas=p).values().first()
            request.session["user_email"]=k["email"]
            return render(request,"index2.html")
        else:
           return HttpResponse("Invalid User!!")
    return render(request,"tlogin.html")

def getview(request,userid):
    k=student.objects.filter(tuitor__email=userid)
    print(k)
    return render(request,"tables.html",{"data":k})
def logout_teacher(request):
    return redirect('home')

def searchstud(request,userid):
    k=student.objects.filter(tuitor__email=userid)
    if request.method=="POST":
        se=request.POST.get('name')
        res=student.objects.filter(fname=se,tuitor__email=userid)
        return render(request,"tables.html",{"data":res})
    return render(request,"tables.html",{"data":k})

def videoadd(request,userid):
    a=department.objects.all()
    if request.method=='POST':
        p=request.FILES['video']
        fo=FileSystemStorage()
        v2=fo.save(p.name,p)
        D=request.POST.get('department')
        s=request.POST.get('semester')
        de=department.objects.get(id=D)
        videos.objects.create(vi=v2,sem=s,depname=de)
    return render(request,"videos.html",{"data":a})

def userhome(request):
    return render(request,"userindex.html")

def login_student(request):
    if request.method=="POST":
        e=request.POST.get('email')
        pa=request.POST.get('password')
        if student.objects.filter(em=e,pas=pa).exists():
            return render(request,"userindex.html")
        else:
           return HttpResponse("Invalid Id!!")
    return render(request,"studlogin.html")

def teacherslist(request):
    return render(request,"teachers.html")
def courselist(request):
    return render(request,"courses.html")




