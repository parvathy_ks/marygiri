from django.shortcuts import render,redirect
from .models import *
from django.core.files.storage import FileSystemStorage
from django.contrib.auth import login,logout,authenticate
from MainApp.models import department,teachers
# Create your views here.
def addstudents(request):
    a=department.objects.all()
    b=teachers.objects.all()
    if request.method=='POST':
        f=request.POST.get('fname')
        l=request.POST.get('lname')
        d=request.POST.get('department')
        e=request.POST.get('email')
        ph=request.POST.get('mobile')
        im=request.FILES['img']
        fo=FileSystemStorage()
        im2=fo.save(im.name,im)
        te=request.POST.get('teacher')
        p=request.POST.get('password')
        de=department.objects.get(id=d)
        tea=teachers.objects.get(id=te)
        student.objects.create(fname=f,lname=l,em=e,mob=ph,dname=de,pas=p,tuitor=tea,profile_pic=im2)
        return redirect("st")
    return render(request,"addstudent.html",{"data":a,"datas":b})
    

def getstudent(request):
    k=student.objects.all()
    if request.method=="POST":
        se=request.POST.get('name')
        res=teachers.objects.filter(name=se)
        return render(request,"slist.html",{"data":res})
    return render(request,"slist.html",{"data":k})

def delstudent(request,userid):
    o=student.objects.get(id=userid)
    o.delete()
    return redirect("st")

def update_student(request,userid):
    x=student.objects.filter(id=userid).values()
    k=department.objects.all()
    if request.method=="POST":
        F=request.POST.get('f_name')
        L=request.POST.get('l_name')
        M=request.POST.get('mobile')
        E=request.POST.get('email') 
        D=request.POST.get('department')
        P=request.POST.get('password')  
        x.update(fname=F,lname=L,mob=M,em=E,dname=D,pas=P)
        return redirect("st")
    return render(request,"studedit.html",{"userdata":x[0],"id":userid,"data":k})




