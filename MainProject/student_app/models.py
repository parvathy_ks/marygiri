from django.db import models


class student(models.Model):
    fname=models.CharField(max_length=40,null=True)
    lname=models.CharField(max_length=40,null=True)
    em=models.EmailField(max_length=30,null=True)
    profile_pic=models.ImageField(upload_to='images/',null=True)
    mob=models.IntegerField(null=True)
    pas=models.CharField(max_length=12,null=True)
    dname=models.ForeignKey('MainApp.department',on_delete=models.CASCADE,null=True)
    tuitor=models.ForeignKey('MainApp.teachers',on_delete=models.CASCADE,null=True)
    def __str__(self):
        return self.fname
